package simple_http

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
)

const (
	REQUEST  = 0
	RESPONSE = 1
)

type Msg struct {
	Type   int `json:"type"`
	ReqMsg `json:"reqMsg"`
	RepMsg `json:"repMsg"`
}

type ReqMsg struct {
	Id     string      `json:"id"`
	Path   string      `json:"path"`
	Params interface{} `json:"param"`
}

type RepMsg struct {
	ReqId string      `json:"reqId"`
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data"`
}

func (m *Msg) Encode() (pkg []byte, err error) {
	pkg, err = json.Marshal(m)
	return
}

func Decode(pkg []byte) (msg Msg, err error) {
	err = json.Unmarshal(pkg, &msg)
	return
}

func IntToBytes8(n int) []byte {
	data := uint64(n)
	byteBuf := bytes.NewBuffer([]byte{})
	binary.Write(byteBuf, binary.BigEndian, data)
	return byteBuf.Bytes()
}

func Bytes8ToInt(bys []byte) int {
	byteBuff := bytes.NewBuffer(bys)
	var data uint64
	binary.Read(byteBuff, binary.BigEndian, &data)
	return int(data)
}
