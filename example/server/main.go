package main

import (
	"gitee.com/it-wind/simple_http"
	"gitee.com/it-wind/simple_http/server"
	"log"
)

func main() {
	s := server.NewTcpServer(":9988")
	s.AddRoute("/upList", UpList)
	s.Run()
}

func UpList(msg simple_http.ReqMsg) simple_http.RepMsg {
	log.Println("action : UpList success")
	return simple_http.RepMsg{
		Code: 0,
		Msg:  "success",
	}
}
