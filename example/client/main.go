package main

import (
	"fmt"
	"gitee.com/it-wind/simple_http"
	"gitee.com/it-wind/simple_http/client"
	"io"
	"log"
)

func main() {
	c := client.NewTcpConn("localhost:9988")
	c.AddRoute("/rem", Remove)
	c.RegisterAction(upList)
	c.Run()
}

func upList(writer io.Writer) {
	msg, err := simple_http.CreateRequest(writer, "/upList", []byte("hello, world!"))
	if err != nil {
		log.Fatalln(err.Error())
	}
	fmt.Println(msg)
}

func Remove(msg simple_http.ReqMsg) simple_http.RepMsg {
	log.Println("action : remove success")
	return simple_http.RepMsg{
		Code: 0,
		Msg:  "success",
	}
}
